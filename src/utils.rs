use ggez::*;

pub fn magnitude_vector2(v: mint::Point2<f32>) -> f32 {
    let result = (v.x * v.x) + (v.y * v.y);
    result.sqrt()
}

pub fn normalize_vector2(v: mint::Point2<f32>) -> mint::Point2<f32> {
    let length = magnitude_vector2(v);
    let x_handler = v.x / length;
    let y_handler = v.y / length;
    mint::Point2 {
        x: x_handler,
        y: y_handler,
    }
}

pub fn add_vectors(v0: mint::Point2<f32>, v1: mint::Point2<f32>) -> mint::Point2<f32> {
    mint::Point2 {
        x: (v0.x + v1.x),
        y: (v0.y + v1.y),
    }
}

pub fn print_vector2(v: mint::Point2<f32>) {
    println!("Vector: {},{}", v.x, v.y);
}
