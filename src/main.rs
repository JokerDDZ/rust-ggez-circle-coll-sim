use ggez::event::{self, EventHandler};
use ggez::graphics::{self, Color};
use ggez::*;
use ggez::{Context, ContextBuilder, GameResult};
use nalgebra as na;
use rand::Rng;

mod circle;
mod utils;
mod wall;

fn main() {
    let wind_mode = ggez::conf::WindowMode {
        width: 1280.0,
        height: 720.0,
        maximized: false,
        fullscreen_type: ggez::conf::FullscreenType::Windowed,
        borderless: false,
        min_width: 0.0,
        max_width: 0.0,
        min_height: 0.0,
        max_height: 0.0,
        resizable: false,
        visible: true,
        resize_on_scale_factor_change: false,
    };

    // Make a Context.
    let (mut ctx, event_loop) = ContextBuilder::new("my_game", "Cool Game Author")
        .window_mode(wind_mode)
        .build()
        .expect("aieee, could not create ggez context!");

    // Create an instance of your event handler.
    // Usually, you should provide it with the Context object to
    // use when setting your game up.
    let my_game = MyGame::new(&mut ctx);

    // Run!
    event::run(ctx, event_loop, my_game);
}

struct MyGame {
    circles: Vec<circle::Circle>,
    circle_pos: mint::Point2<f32>,
    top:f32,
    bot:f32,
    left:f32,
    right:f32,
}

impl MyGame {
    pub fn new(_ctx: &mut Context) -> MyGame {
        // Load/create resources such as images here.
        let (screen_w, screen_h) = graphics::drawable_size(_ctx);
        let (screen_w_b,screen_h_b) = (0.0,0.0);
        let (h_s_w, h_s_h) = (screen_w / 2.0, screen_h / 2.0);

        let mut result = MyGame {
            circle_pos: mint::Point2 { x: h_s_w, y: h_s_h },
            circles: Vec::with_capacity(80),
            top:0.0,
            bot:screen_h,
            left:0.0,
            right:screen_w,
        };

        result.randomize_start_pos_of_circles();
        result
    }

    pub fn draw_all(&mut self, _ctx: &mut Context) -> GameResult<()> {
        for x in &self.circles {
            draw_circle(_ctx, x.clone())?;
        }

        Ok(())
    }

    pub fn randomize_start_pos_of_circles(&mut self) {
        let mut rng = rand::thread_rng();

        for _n in 1..80 {
            //random pos in X
            let x = self.circle_pos.x + rng.gen_range(-500.0..500.0);
            let y = self.circle_pos.y;
            let mut c: circle::Circle = circle::Circle::new();
            c.pos.x = x;
            c.pos.y = y;

            //random dir
            let dir_x = rng.gen_range(-1.0..1.0);
            let dir_y = rng.gen_range(-1.0..1.0);
            let norm_vector: mint::Point2<f32> =
                utils::normalize_vector2(mint::Point2 { x: dir_x, y: dir_y });
            c.dir = norm_vector;

            //random speed
            c.speed = rng.gen_range(50.0..300.0);

            self.circles.push(c);
        }
    }

    pub fn move_circles(&mut self, delta: f32) {
        for x in &mut self.circles {
            let handler = delta * x.speed;
            let vector_handler = mint::Point2 {
                x: x.dir.x * handler,
                y: x.dir.y * handler,
            };

            x.pos = utils::add_vectors(vector_handler, x.pos);

            if (x.pos.y - x.radius) < self.top || (x.pos.y + x.radius) > self.bot{
                x.dir.y = -x.dir.y;
            } else if (x.pos.x - x.radius) < self.left || (x.pos.x + x.radius) > self.right{
                x.dir.x = -x.dir.x;
            }
        }
    }
}

pub fn draw_circle(_ctx: &mut Context, c: circle::Circle) -> GameResult<()> {
    let circle = graphics::Mesh::new_circle(
        _ctx,
        graphics::DrawMode::fill(),
        c.pos,
        c.radius,
        0.1,
        c.color,
    )?;
    let circe_1_draw_param = graphics::DrawParam::default();
    graphics::draw(_ctx, &circle, circe_1_draw_param)?;
    Ok(())
}

impl EventHandler for MyGame {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        let dt = ggez::timer::delta(_ctx);
        //self.circle_pos.y += dt.as_secs_f32() * 10.0;
        self.move_circles(dt.as_secs_f32());
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, Color::WHITE);
        self.draw_all(ctx)?;
        graphics::present(ctx)?;
        Ok(())
    }
}
