use ggez::*;

#[derive(Copy, Clone)]
pub struct Circle {
    pub pos: mint::Point2<f32>,
    pub radius: f32,
    pub color: graphics::Color,
    pub speed:f32,
    pub dir: mint::Point2<f32>,
    pub acceleration: mint::Point2<f32>,
}

impl Circle {
    pub fn new() -> Circle {
        Circle {
            pos: mint::Point2 { x: 0.0, y: 0.0 },
            radius: 15.0,
            color: graphics::Color::BLACK,
            dir: mint::Point2 { x: 0.0, y: 0.0 },
            acceleration: mint::Point2 { x: 0.0, y: 0.0 },
            speed:10.0,
        }
    }
}
